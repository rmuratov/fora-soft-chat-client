# Demo

(при первом заходе может грузиться несколько секунд, ограничение бесплатного Heroku)

https://chat.rmuratov.ru

# Dev

Run

```shell
cd client
npm install
```

Create .env file with socket.io host, i.e.:

```
REACT_APP_SOCKET_IO_HOST=http://localhost:3001
```

Run `npm start`.
Go to localhost:3000

# Build

```shell
npm run build # => 'public' folder
```
