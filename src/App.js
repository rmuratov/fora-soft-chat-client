import React, { Component } from 'react';
import './App.css';
import ChatRoom from './ChatRoom.js';
import ChooseName from './ChooseName.js';
import Header from './Header';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { name: false };
    this.setName = this.setName.bind(this);
  }

  setName(name) {
    this.setState({ name });
  }

  render() {
    return (
      <div className="container">
        <Header />
        {this.state.name ? (
          <ChatRoom name={this.state.name} />
        ) : (
          <ChooseName handleSubmit={this.setName} />
        )}
      </div>
    );
  }
}

export default App;
