import io from 'socket.io-client';

import {
  NEW_MESSAGE,
  USER_JOINED_THE_ROOM,
  USER_LEFT_THE_ROOM,
  ROOM,
  JOIN,
  CONNECT,
  ERROR,
  CONNECT_ERROR,
  CONNECT_TIMEOUT,
  DISCONNECT,
  RECONNECT,
  RECONNECT_ATTEMPT,
  RECONNECTING,
  RECONNECT_ERROR,
  RECONNECT_FAILED,
  PING,
  PONG,
} from './eventNames.js';

class ChatClient {
  constructor(host) {
    this.socket = io(host);
  }

  joinRoom(room, name) {
    this.socket.emit(JOIN, { room, name });
  }

  sendMessage(name, text) {
    this.socket.emit(NEW_MESSAGE, { name, text });
  }

  handleJoined(cb) {
    this.socket.on(USER_JOINED_THE_ROOM, cb);
  }

  handleLeft(cb) {
    this.socket.on(USER_LEFT_THE_ROOM, cb);
  }

  handleRoom(cb) {
    this.socket.on(ROOM, cb);
  }

  handleNewMessage(cb) {
    this.socket.on(NEW_MESSAGE, cb);
  }

  // Predefined events
  handleConnect(cb) {
    this.socket.on(CONNECT, cb);
  }

  handleError(cb) {
    this.socket.on(ERROR, cb);
  }

  handleConnectError(cb) {
    this.socket.on(CONNECT_ERROR, cb);
  }

  handleConnectTimeout(cb) {
    this.socket.on(CONNECT_TIMEOUT, cb);
  }

  handleDisconnect(cb) {
    this.socket.on(DISCONNECT, cb);
  }

  handleReconnect(cb) {
    this.socket.on(RECONNECT, cb);
  }

  handleReconnectAttempt(cb) {
    this.socket.on(RECONNECT_ATTEMPT, cb);
  }

  handleReconnecting(cb) {
    this.socket.on(RECONNECTING, cb);
  }

  handleReconnectError(cb) {
    this.socket.on(RECONNECT_ERROR, cb);
  }

  handleReconnectFailed(cb) {
    this.socket.on(RECONNECT_FAILED, cb);
  }

  handlePing(cb) {
    this.socket.on(PING, cb);
  }

  handlePong(cb) {
    this.socket.on(PONG, cb);
  }
}

export default ChatClient;
