import React, { Component } from 'react';
import ChatClient from './ChatClient.js';
import LogViewer from './LogViewer.js';
import MessageForm from './MessageForm.js';
import MessagesList from './MessagesList.js';
import OnlineList from './OnlineList.js';

/**
 * Комната. Рендерится после ввода имени.
 * Здесь вся логика отправки и приема сообщений.
 */
class ChatRoom extends Component {
  constructor(props) {
    super(props);
    this.state = { messages: [], connected: false, members: [], log: [] };
    this.name = props.name;
    this.client = new ChatClient(process.env.REACT_APP_SOCKET_IO_HOST);

    this.sendMessage = this.sendMessage.bind(this);
    this.handleConnect = this.handleConnect.bind(this);
    this.handleJoined = this.handleJoined.bind(this);
    this.handleRoom = this.handleRoom.bind(this);
    this.handleLeft = this.handleLeft.bind(this);
    this.handleNewMessage = this.handleNewMessage.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  sendMessage(text) {
    this.client.sendMessage(this.name, text);
  }

  handleConnect() {
    this.setState({ connected: true, log: [...this.state.log, 'connected'] });
  }

  handleJoined(newMemeber) {
    this.setState({ members: [...this.state.members, { ...newMemeber }] });
  }

  handleLeft({ id }) {
    this.setState({
      members: this.state.members.filter(member => member.id !== id),
    });
  }

  handleRoom({ room: { id, members, messages } }) {
    window.history.pushState('', '', `/?room=${id}`);
    this.setState({ members, messages });
  }

  handleNewMessage({ name, text, dateTime }) {
    this.setState({
      messages: [...this.state.messages, { name, text, dateTime }],
    });
  }

  handleError(error) {
    this.setState({ log: [...this.state.log, 'something went wrong'] });
    console.log(error);
  }

  componentDidMount() {
    const room = new URLSearchParams(window.location.search).get('room');

    this.client.joinRoom(room, this.name);

    this.client.handleConnect(this.handleConnect);
    this.client.handleError(this.handleError);

    this.client.handleJoined(this.handleJoined);
    this.client.handleRoom(this.handleRoom);
    this.client.handleLeft(this.handleLeft);
    this.client.handleNewMessage(this.handleNewMessage);
  }

  render() {
    if (!this.state.connected) return <p>Connection...</p>;
    return (
      <div className="row">
        <div className="column column-60">
          <MessagesList messages={this.state.messages} />
          <MessageForm handleSubmit={this.sendMessage} />
        </div>
        <div className="column column-20">
          <OnlineList members={this.state.members} />
        </div>
        <div className="column column-20">
          <LogViewer log={this.state.log} />
        </div>
      </div>
    );
  }
}

export default ChatRoom;
