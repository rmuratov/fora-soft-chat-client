import React, { Component } from 'react';

/** Компонент выбора никнейма для чата */
class ChooseName extends Component {
  constructor(props) {
    super(props);
    this.state = { input: '' };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ input: event.target.value.trim() });
  }

  handleSubmit(event) {
    this.props.handleSubmit(this.state.input);
    this.setState({ input: '' });
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <fieldset>
          <input
            type="text"
            required
            placeholder="Your name"
            value={this.state.input}
            onChange={this.handleChange}
          />
          <div className="float-right">
            <input
              disabled={!this.state.input}
              className="button-primary"
              type="submit"
              value="Go"
            />
          </div>
        </fieldset>
      </form>
    );
  }
}

export default ChooseName;
