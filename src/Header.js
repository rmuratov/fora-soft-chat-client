import React from 'react';

const Header = () => (
  <h1 className="logo">
    <strong>
      <span role="img" aria-label="logo">
        🦜
      </span>{' '}
      Chat
    </strong>
  </h1>
);

export default Header;
