import React from 'react';

/** Для отображения различных событий */
const LogViewer = ({ log }) => (
  <>
    <h3>Status:</h3>
    <ul>
      {log.map((item, i) => (
        <li key={i}>{item}</li>
      ))}
    </ul>
  </>
);

export default LogViewer;
