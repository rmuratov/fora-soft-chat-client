import React from 'react';

const MessageItem = ({ dateTime, name, text }) => (
  <li>
    {new Date(dateTime).toLocaleTimeString('ru-RU')} <b>{name}</b>: {text}
  </li>
);

/** Отрисовка сообщений */
const MessagesList = ({ messages }) => {
  if (messages.length === 0) {
    return (
      <div className="messages-box">
        <p>No messages yet...</p>
      </div>
    );
  }
  return (
    <div className="messages-box">
      <ul className="messages-list">
        {messages.map(message => (
          <MessageItem
            {...message}
            key={`${message.dateTime}${message.text}`}
          />
        ))}
      </ul>
    </div>
  );
};

export default MessagesList;
