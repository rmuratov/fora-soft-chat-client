import React from 'react';

/** Список людей в комнате */
const OnlineList = ({ members }) => (
  <>
    <h3>Online:</h3>
    <ul>
      {members.map(el => (
        <li key={el.id}>{el.name}</li>
      ))}
    </ul>
  </>
);

export default OnlineList;
