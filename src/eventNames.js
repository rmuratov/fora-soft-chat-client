export const NEW_MESSAGE = 'NEW_MESSAGE';
export const USER_JOINED_THE_ROOM = 'USER_JOINED_THE_ROOM';
export const USER_LEFT_THE_ROOM = 'USER_LEFT_THE_ROOM';
export const ROOM = 'ROOM';
export const JOIN = 'JOIN';

/** Predefined events */
export const CONNECT = 'connect';
export const CONNECT_ERROR = 'connect_error';
export const CONNECT_TIMEOUT = 'connect_timeout';
export const ERROR = 'error';
export const DISCONNECT = 'disconnect';
export const RECONNECT = 'reconnect';
export const RECONNECT_ATTEMPT = 'reconnect_attempt';
export const RECONNECTING = 'reconnecting';
export const RECONNECT_ERROR = 'reconnect_error';
export const RECONNECT_FAILED = 'reconnect_failed';
export const PING = 'ping';
export const PONG = 'pong';
